import ReactDOM from 'react-dom';
import React from 'react';
import './index.css';
import App from './main/app';

ReactDOM.render(
  <div>
    <App />
  </div>

  , document.getElementById('root')
);
