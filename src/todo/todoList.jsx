import React from 'react';
import IconButton from '../template/iconButton';

const TodoList = (props) => {
  const renderRows = () => {
    const list = props.list || [];
    return list.map((tarefa) => {
      return (
        <tr key={tarefa._id}>
          <td className={tarefa.done ? 'markedAsDone' : ''}>{tarefa.description}</td>
          <td>
            <IconButton
              color='danger'
              icon='trash'
              onClick={() => props.onRemove(tarefa._id)}
            />
            <IconButton
              color='success'
              icon='check'
              hide={tarefa.done}
              onClick={() => props.onDone(tarefa._id)}
            />
            <IconButton
              color='warning'
              icon='undo'
              hide={!tarefa.done}
              onClick={() => props.onPending(tarefa._id)}
            />
          </td>
        </tr>
      );
    });
  };
  return (
    <table className='table'>
      <thead>
        <tr key='HeadRow'>
          <th>Descrição</th>
          <th className='tableActions'>Ações</th>
        </tr>
      </thead>
      <tbody>
        {renderRows()}
      </tbody>
    </table>
  );
};

export default TodoList;
