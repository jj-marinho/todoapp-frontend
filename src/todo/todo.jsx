import React from 'react';
import PageHeader from '../template/pageHeader';
import TodoForm from './todoForm';
import TodoList from './todoList';
import axios from 'axios';
import './todoList.css';

// URL do backend
const backURL = 'http://localhost:5000/api/todos';

export default class Todo extends React.Component {
  constructor (props) {
    super();
    this.state = {
      description: '',
      list: []
    };
    this.refresh();
  }

  handleAdd = () => {
    const description = this.state.description;
    axios.post(backURL, {
      description
    })
      .then(response => console.log('Status: ', response.status, ' Data: ', response.data))
      .catch(error => console.log(error))
      .finally(() => this.refresh());
  }

  handleClear = () => {
    this.setState({
      ...this.state,
      description: ''
    });
  }

  handleRemove = (idTarefa) => {
    axios.delete(`${backURL}/${idTarefa}`)
      .then(response => console.log('Status: ', response.status, ' Data: ', response.data))
      .catch(error => console.log(error.data))
      .finally(() => this.refresh());
  }

  handleDone = (idTarefa) => {
    axios.put(`${backURL}/${idTarefa}`, {
      done: true
    })
      .then(response => console.log('Status: ', response.status, ' Data: ', response.data))
      .catch(error => console.log(error))
      .finally(() => this.refresh());
  }

  handlePending = (idTarefa) => {
    axios.put(`${backURL}/${idTarefa}`, {
      done: false
    })
      .then(response => console.log('Status: ', response.status, ' Data: ', response.data))
      .catch(error => console.log(error))
      .finally(() => this.refresh());
  }

  handleChange = (event) => {
    this.setState({
      ...this.state,
      description: event.target.value
    });
  }

  refresh = () => {
    axios.get(backURL)
      .then(response => {
        this.setState({
          ...this.state,
          description: '',
          list: response.data
        });
        return response;
      })
      .catch(error => console.log(error));
  }

  render = () => (
    <div>
      <PageHeader name='Tarefas' small='Cadastro' />
      <TodoForm
        description={this.state.description}
        onAdd={this.handleAdd}
        onChange={this.handleChange}
        onClear={this.handleClear}
      />
      <TodoList
        list={this.state.list}
        onRemove={this.handleRemove}
        onPending={this.handlePending}
        onDone={this.handleDone}
      />
    </div>
  )
}
