import React from 'react';

const IconButton = props => {
  if (props.hide) {
    return null;
  }
  return (
    <button
      className={'btn btn-' + props.color}
      onClick={props.onClick}
    >
      <i className={'fas fa-' + props.icon} />
    </button>
  );
};

export default IconButton;
