import React from 'react';

const menu = (props) => {
  return (
    <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
      <div className='container'>
        <div className='navbar-header'>
          <a className='navbar-brand' href='#/'>
            <i className='fas fa-calendar-check' /> TodoApp
          </a>
        </div>
        <div id='navbar' className='navbar-collapse collapse'>
          <ul className='navbar-nav mr-auto'>
            <li className='nav-item'><a className='nav-link' href='#/todos'>Tarefas</a></li>
            <li className='nav-item'><a className='nav-link' href='#/about'>Sobre</a></li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default menu;
