import 'bootstrap/dist/css/bootstrap.min.css';
import '@fortawesome/fontawesome-free/js/all';
import Menu from '../template/menu';
import Routes from './routes';

import React from 'react';

export default props => (
  <div className='container'>
    <Menu />
    <Routes />
  </div>
);
