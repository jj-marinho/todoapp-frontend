import React from 'react';
import Grid from '../template/grid';
import IconButton from '../template/iconButton';

const TodoForm = props => {
  return (
    <form className='todoForm'>
      <div className='row'>
        <Grid cols='12 9 10'>
          <input
            id='description'
            className='form-control'
            placeholder='Adicione uma tarefa'
            value={props.description}
            onChange={(event) => props.onChange(event)}
          />
        </Grid>

        <Grid cols='12 3 2'>
          <IconButton color='primary' icon='plus' onClick={props.onAdd} />
          <IconButton color='info' icon='search' onClick={props.onSearch} />
          <IconButton color='dark' icon='times' onClick={props.onClear} />
        </Grid>
      </div>

    </form>
  );
};

export default TodoForm;
